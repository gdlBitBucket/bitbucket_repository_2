﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.Cameras; // gdl to access frrelook cam
//using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ManagerBe : MonoBehaviour {
	 
	public GameObject target;
	public Camera iCam;
	//public GameObject objToMove;
	public FreeLookCam lookCam;

	public GameObject model1;
	public GameObject model2;
	private GameObject model_current;

	public ParseHierarchy animBe;


	public GameObject targetViewRoot;

	public Toggle uiToggle;
	public Material targetMaterial;
	public Material matNominal;
	public Material matXSection;

	public Button uiPlayPause;
	public Sprite spritePlay;
	public Sprite spritePause;

	public GameObject ui_xsection;
	public GameObject ui_slomo;

	public animTextureMaskBe uiEKG;

	private bool wasPlaying=true;
	public bool isEKGdown=false;

	private float perspectiveZoomSpeed = 0.12f;        // The rate of change of the field of view in perspective mode.
	private Vector4 vecXPlane;

	// Use this for initialization
	void Start () {
		/*EventTrigger trigger = uiToggle.gameObject.GetEventTrigger();
		trigger.AddEventListener(EventTriggerType.PointerDown, TestA);
		trigger.AddEventListener(EventTriggerType.PointerUp, TestB);		*/
		//uiToggle.onValueChanged.AddListener(delegate{onToggle();});
		//uiPlayPause.onClick.AddListener(delegate{onPlayPause();});

		/*Image img = uiPlayPause.GetComponent<UnityEngine.UI.Image>();
		img.overrideSprite = animBe.isPaused ? spritePlay : spritePause;*/

		model1.SetActive (true);
		model2.SetActive (false);
		model_current = model1;
		animBe = model_current.GetComponent<ParseHierarchy>();

	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown("escape")) {//When a key is pressed down it see if it was the escape key if it was it will execute the code
			Application.Quit(); // Quits the game
		}


		if (animBe == null) {
		
			model1.SetActive (true);
			model2.SetActive (false);
			model_current = model1;
			animBe = model_current.GetComponent<ParseHierarchy>();
		}


		if (Input.touchCount == 1) { 	
			Touch touch = Input.GetTouch (0);			
			if (touch.phase == TouchPhase.Moved) {
				Vector2 touchDeltaPosition = touch.deltaPosition; 
				//textX.text = (touchDeltaPosition.x).ToString(); //*100f; //*0.1f;
				//textY.text = (touchDeltaPosition.y).ToString();				
				//deltaXprev=touchDeltaPosition.x;
				//deltaYprev=touchDeltaPosition.y;

				if(isEKGdown)
				{
					animBe.commandMoveStep=0.75F*touchDeltaPosition.x;
					//textX.text = animBe.commandMoveStep.ToString();
				}			
			}
		}

		// test debug
		/*		if (uiToggle.isOn) {	
			Vector4 vector = targetMaterial.GetVector ("_section");
			Debug.Log ("x=" + vector.x.ToString()+"w=" + vector.w.ToString());

			Vector4 vec;
			vec.x = 50f;			vec.y = 10f;			vec.z = 5f;			vec.w = vector.w+0.003f;
			targetMaterial.SetVector ("_section", vec);
			//Vector4 vector = targetMaterial.GetVector ("_section");
			//float ww = vector.w;
			//Debug.Log ("x=" + vector.x.ToString()+"w=" + ww);

			//vecXPlane.x=45f; vecXPlane.y=0f; vecXPlane.z=0f; vecXPlane.w=-0.5f; 			
			//targetMaterial.SetVector ("_section", vec);  // no "get" -> bug
			Vector4 vector2 = targetMaterial.GetVector ("_section");
			Debug.Log ("x=" + vector2.x.ToString()+"w=" + vector2.w.ToString());
			}			*/

		// gdl touch
		//float speed = 0.1F;
		if(Input.touchCount>=2)
		{
			// zoom
			/*Vector2 touchDeltaPosition0 = Input.GetTouch(0).deltaPosition;
				Vector2 touchDeltaPosition1 = Input.GetTouch(1).deltaPosition;
				float fzoom=(touchDeltaPosition1-touchDeltaPosition0).magnitude*0.1f;;
				m_Target.transform.Translate(0,0,fzoom);		*/
			
			// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);
			
			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			
			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
			
			// Otherwise change the field of view based on the change in distance between the touches.
			iCam.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
			
			// Clamp the field of view to make sure it's between 0 and 180.
			iCam.fieldOfView = Mathf.Clamp(iCam.fieldOfView, 0.1f, 179.9f);

			//textAngle.text =deltaMagnitudeDiff.ToString();
			//if(deltaMagnitudeDiff<10)
			{
				// drive w cutting plane
				if(uiToggle.isOn) // cutting plane mode on
				{
					Vector4 vector = targetMaterial.GetVector ("_section");
					Debug.Log ("x=" + vector.x.ToString()+"w=" + vector.w.ToString());

					float speed = touchZero.deltaPosition.x / 50F;

					// follow hand motion

					if (targetViewRoot.transform.localEulerAngles.y > 90 && targetViewRoot.transform.localEulerAngles.y < 270)
						speed = -speed;

					Vector4 vec;
					vec.x = 45f;	vec.y = 2f;	vec.z = 5f;	vec.w = vector.w-speed;
					vec.w= Mathf.Clamp(vec.w, -2f, 2f);

					targetMaterial.SetVector ("_section", vec);

					//textX.text = wXplane_prev.ToString(); 
					//wXplane_prev +=touchZero.deltaPosition.x/10F;
					//textY.text=touchZero.deltaPosition.x.ToString();
					//wXplane_prev= Mathf.Clamp(wXplane_prev, -2f, 2f);
					//vecXPlane.w=wXplane_prev;
					//textY.text=wXplane_prev.ToString();
					//vecXPlane.x=45f; vecXPlane.y=0f; vecXPlane.z=0f; vecXPlane.w=-0.5f; 
					//targetMaterial.SetVector("_section)", vecXPlane);  // no "get" -> bug
				}
			}				
		}
		// test


		//Debug.DrawLine (iCam.transform.position, objToMove.transform.position, Color.red);

			foreach (Touch touch  in Input.touches)
			{
				/*if (touch.phase == TouchPhase.Began) {
					// Construct a ray from the current touch coordinates					
					objToMove.transform.Rotate(0f,30f,0f);
				}*/

			/*	if (touch.phase == TouchPhase.Moved) {
					objToMove.transform.Rotate(0f,2f,0f);
				}*/
			}

	
	}




	public void onToggle (GameObject button) {
			
		Debug.Log ("*** TOGGLE CHANGED ***" + uiToggle.isOn);
		//objToMove.transform.Rotate(0f,0f,5f);

		if (uiToggle.isOn) {
			targetMaterial.shader = matXSection.shader;
			targetMaterial.CopyPropertiesFromMaterial (matXSection);
			//vecXPlane=matXSection.GetVector("_section");
			//wXplane_prev=vecXPlane.w;			
		} 
		else 
		{
			targetMaterial.shader = matNominal.shader;
			targetMaterial.CopyPropertiesFromMaterial (matNominal);	
		}
			
	}

	// PLAY / PAUSE UI
	public void onPlayPause (GameObject button) 
	{
		// buttonGameObject.GetComponent<UnityEngine.UI.Image>();
		animBe.isPaused = !animBe.isPaused;

		Image img = button.GetComponent<UnityEngine.UI.Image>();

		img.overrideSprite = animBe.isPaused ? spritePlay : spritePause;
		
		Debug.Log ("*** UI Play PAUSE ***" + uiToggle.isOn);	
	}

	//
	// EKG Slider
	//
	public void onEKGpointer_down (GameObject button) 
	{
		if (animBe.isPaused == false) {
			wasPlaying = true;
			animBe.isPaused = true;

			Image img = uiPlayPause.GetComponent<UnityEngine.UI.Image> ();			
			img.overrideSprite = animBe.isPaused ? spritePlay : spritePause;
		} 
		else
			wasPlaying = false;

		isEKGdown = true;
		uiEKG.isTouched = isEKGdown;

		lookCam.lockMove = true;

		Debug.Log ("*** onEKGpointer_down ***" + uiToggle.isOn);
	}


	public void onEKGpointer_up (GameObject button) 
	{	
		if (wasPlaying == true) {
			animBe.isPaused = false;
			Image img = uiPlayPause.GetComponent<UnityEngine.UI.Image> ();			
			img.overrideSprite = animBe.isPaused ? spritePlay : spritePause;
		}

		isEKGdown = false;

		uiEKG.isTouched = isEKGdown;

		lookCam.lockMove = false;

		Debug.Log ("*** onEKGpointer_up ***" + uiToggle.isOn);
	
	}


	public void switchModel()
	{
		model1.SetActive (!model1.activeInHierarchy);
		model2.SetActive (!model1.activeInHierarchy);


		if (model1.activeInHierarchy) {
			model_current = model1;
			ui_xsection.SetActive (true);
			ui_slomo.SetActive (true);
		}
		else {
			model_current = model2;
			ui_xsection.SetActive (false);
			ui_slomo.SetActive (false);
		}
	
	animBe = model_current.GetComponent<ParseHierarchy>();
	}

	public void toggleSloMo()
	{
	if(animBe != null)
		animBe.isSloMo = !animBe.isSloMo;
	}


}
