﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ParseHierarchy : MonoBehaviour {

	//public string myName;
	//public float animTimer_ms=80;
	public int oStep;
	public int oCycleCount=0;

	public string baseName;

	public bool isPaused;
	public float 	commandMoveStep;
	public bool isSloMo;

	//public bool isKinectDriven;

	private List<Transform> ts = new List<Transform>();
	// public List<Vector3> ts_seat = new List<Vector3>();

	public FPS fpsBe;

	// // // //
	private int size=0;
	private List<Transform> ts0 = new List<Transform>();

	private Transform[] tsarray ; //= new List<Transform>();

	private int displayedIdx=0;
	private int maxIndex = 0;
	private int tsize=0;

	//private float elapsed=0;
	//private float timePrev=0;
	private int frameCount=0;

	private float stepIncr=1F;
	private int idxPrev=0;

	private AudioSource cAudioSource;



	// Use this for initialization
	void Start () {

		//Debug.Log("*****************   INITIALIZING ... " + myName);
		/*foreach (GameObject obj in Object.FindObjectsOfType(typeof(GameObject)))
		{
			if(obj.name.Contains("Taurus"))
				Debug.Log(obj.name);
		}*/
		frameCount = 0;
		isSloMo = false;

		cAudioSource = gameObject.GetComponent<AudioSource>();

		Debug.Log("*** *** *** *** *** Parsing children through transform *** *** ***  *** ***" );

		foreach (Transform child in transform)
		{
			if(child.name.Contains(baseName))
			{
				ts0.Add(child);
				size++;
				Debug.Log(child.name + " "+ size);

				string lastbit=child.name.Replace(baseName,"");
				int index=int.Parse(lastbit);
				if(index>maxIndex) maxIndex=index;
				//Debug.Log(lastbit);
			}
		}


		tsarray = new Transform[maxIndex+1];

		//int min = 0; int max = 10000;
		foreach (Transform child in ts0) 
		{
			string lastbit=child.name.Replace(baseName,"");
			int index=int.Parse(lastbit);
			Debug.Log(lastbit);

			tsarray[index]=child;
		}



		for (int i = 0; i < tsarray.Length; i++) 
		{
			if (tsarray[i]!=null)
			{
				ts.Add(tsarray[i]);
				Transform tr = tsarray[i];
				tr.gameObject.SetActive (false); // hide all
			}
		}

		ts0.Clear ();
		tsize = ts.Count;

		// Get P41001 (seat) objects to BB position
		/* for (int i = 0; i < ts.Count; i++) 
		{
			String name=ts[i].name;
			Debug.Log(" For ..."+ name);
			//foreach (Transform child in ts[i])
			{
				String slookFor="P41001";
				GameObject obj = GoGetIt(ts[i].gameObject, slookFor);
				if(obj)
				{
					MeshRenderer mrend=obj.gameObject.GetComponent<MeshRenderer>();
					//Vector3 pos = Traverse(child);; //Vector3.zero;
					//Traverse(child);
					ts_seat.Add (mrend.bounds.center);
				}
				else{	Debug.Log(" P41001 NOT FOUND !!!");
				}
			}
		}		*/
	}



	Vector3 Traverse(Transform obj)
	{ 
		//if (oPosition != Vector3.zero) 			return ;
		//Debug.Log(obj.name); 
		if (obj.name == "P41001") {
			Debug.Log("--Traverse :/: Found");

			MeshRenderer mrend=obj.gameObject.GetComponent<MeshRenderer>();
			//mrend.bounds
			return mrend.bounds.center;
		}
		else 
		{
			foreach (Transform child in obj.transform) { 
				return Traverse (child); 
			}
			return Vector3.zero;
		}
	}


	GameObject GoGetIt( GameObject go, string name)
	{
		go.SetActive (true);

		Transform ot = null;
		foreach (Transform x in go.GetComponentsInChildren<Transform>())
		{
			//String dd= x.name;
			if (x.gameObject.name == name)
			{
				ot = x;
				break;
			}
		}
		//go.SetActive (true);
		go.SetActive (false);
		if(ot)
			return ot.gameObject;
		else 
		{
			return null;
		}
		//throw new System.Exception("Technically the old version throws an exception if none are found, so I'll do the same here!");
	}



	// Update is called once per frame
	void Update () {

		// TIMER
		/*	elapsed = (Time.time - timePrev)*1000F;
		if (elapsed < animTimer_ms) 
			return; */

		// 25 frames (tsize)

		float timeRatio = fpsBe.fps / tsize;
		print ("timeRatio: "+ timeRatio);

		int frameWait = Mathf.RoundToInt(fpsBe.fps) / tsize;
		//int frameWait = Mathf.RoundToInt(fpsBe.fpsDebug) / tsize;
		//print ("frameWait: "+ frameWait);
		fpsBe.text.text = "FPS " +Mathf.RoundToInt(fpsBe.fps) + "-" + frameWait;

		frameCount++;
		if (frameCount <= frameWait)
			return;
		frameCount = 0;

		// Pause management
		if (isPaused) 
		{
			if(commandMoveStep==0F)
				return;
			else stepIncr = commandMoveStep;
		}
		else{
			stepIncr=1F;
		}


		//int frameIncrement = 1;

		// SloMo (all frames ) or 1s per cycle :
		if (!isSloMo) {
			if (timeRatio < 1F) {
				stepIncr = Mathf.RoundToInt (1F / timeRatio); 
				print ("stepIncr: " + stepIncr.ToString ());
			}
		}



		//timePrev = Time.time;

		//int idxPrev = 0;
		/*if ((displayedIdx > 0) && (displayedIdx <= tsize)) {
			idxPrev = displayedIdx - stepIncr;
		} else if (displayedIdx == 0) {
			idxPrev = tsize - 1;		
			Debug.Log("--- Beat Cycle ... ");
		}			*/

		Transform tr = ts [displayedIdx];
		tr.gameObject.SetActive (true);

		//Debug.Log("** index : " + displayedIdx.ToString()+"  idx prev" + idxPrev + tr.gameObject.name);
		if(displayedIdx!=idxPrev)
		{
			tr = ts [idxPrev];
			tr.gameObject.SetActive (false);
		}

		idxPrev=displayedIdx;

		displayedIdx +=(int)stepIncr;

		if(displayedIdx<0) {
			displayedIdx=tsize-1;
			oCycleCount--;
		}

		if (displayedIdx >= tsize)
		{
			displayedIdx = 0;
			oCycleCount++;

			if (cAudioSource != null)
				cAudioSource.Play();
		}

		oStep=displayedIdx;


		commandMoveStep=0F;
	

	}
}
