﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class animTextureMaskBe : MonoBehaviour {

	//public Material targetMaterial;
	public float step=0.01F;
	public float min=-0.7f;
	public float max=-0.06f;

	public bool isTouched=false;

	//public ParseHierarchy animBe;
	public ManagerBe manager;

	private int animStepMax = 100;

	private int count=0;
	private int stepPrev=0;

	private RawImage rawimg;

	//Renderer rend;


	// Use this for initialization
	void Start () {
		stepPrev = manager.animBe.oStep;	

		rawimg = GetComponent<RawImage>();
	}
	
	// Update is called once per frame
	void Update () {

		if (manager.animBe == null)
			return;

		if (rawimg == null) {
			
			rawimg = GetComponent<RawImage> ();
			return;
		}

		int deltaStep = stepPrev - manager.animBe.oStep;
		if (deltaStep == 0)
			return;

		//Vector2 vectorOffset =	targetMaterial.GetTextureOffset("_MainTex");

		float newX; //= vectorOffset.x - step;

		// -0.33  -0.66 -0.99  //
		// step : from 0 to 100
		//Debug.Log("X texture : "+vectorOffset.x.ToString());
		// sense the direction

		count = modulo (manager.animBe.oCycleCount,  3) ;

	/*	if (isTouched == false) {
			if (animBe.oStep >= animStepMax - 1)
				count++;
		} else {				 
			if(Mathf.Sqrt(deltaStep)>Mathf.Sqrt(animStepMax/2))
				count--;		
			else
				if (animBe.oStep >= animStepMax - 1) count++;
		}*/

		newX = (manager.animBe.oStep*1.0F/animStepMax * -0.33F) + (-0.33F*count);


		// if (count>=1 && animBe.oStep == 0 )	count--;
	/*	if (count > 2 || count<0) // || isTouched==true)
			count = 0; */

		if (newX < min)
			newX = max;

		//newX= Mathf.Clamp(newX, -0.65f, -0.06f);

		//vectorOffset.x = newX;

		Rect rr = rawimg.uvRect;
		rr.x = newX;

		//GetComponent<RawImage>().uvRect = new Rect(X, Y, W, H);
		//targetMaterial.SetTextureOffset ("_MainTex", vectorOffset);
		rawimg.uvRect = rr;

		stepPrev = manager.animBe.oStep;	

		//print (" Mask anim newX :" + newX);
	}


	private int modulo( int dividend,  int divisor) 
	{
		return (((dividend) % divisor) + divisor) % divisor;
	}
	
	
	
}
